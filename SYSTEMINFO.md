# Arista CloudVision Portal

Vendor: Arista Networks
Homepage: https://www.arista.com/en/

Product: CloudVision Portal
Product Page: https://www.arista.com/en/cg-cv/cv-cloudvision-portal-cvp-overview

## Introduction
We classify CloudVision Portal into the Data Center domain as CloudVision Portal provides capabilities in provisioning, configuration, and monitoring of data center resources. We also classify CloudVision Portal into the Network Services domain as it contributes to managing and delivering network services across the infrastructure.

## Why Integrate
The CloudVision Portal adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Arista CloudVision Portal. 

With this adapter you have the ability to perform operations with CloudVision Portal such as:

- Get Inventory
- Execute Confliglet
- Get Event
- Provision

## Additional Product Documentation
The [API documents for Arista CloudVision Portal](https://arista.my.site.com/AristaCommunity/s/article/Understanding-CloudVIsion-APIs-and-accessing-NetDB-data)