## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for CloudVision Portal. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for CloudVision Portal.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Arista CloudVision Portal. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createServerDo(body, callback)</td>
    <td style="padding:15px">createServer.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/createServer.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServerDo(body, callback)</td>
    <td style="padding:15px">deleteServer.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/deleteServer.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editServerDo(body, callback)</td>
    <td style="padding:15px">editServer.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/editServer.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAAADetailsByIdDo(id, callback)</td>
    <td style="padding:15px">getAAADetailsById.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/getAAADetailsById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerByIdDo(id, callback)</td>
    <td style="padding:15px">getServerById.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/getServerById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServersDo(serverType, queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getServers.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/getServers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveAAADetailsDo(body, callback)</td>
    <td style="padding:15px">saveAAADetails.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/saveAAADetails.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testServerConnectivityDo(body, callback)</td>
    <td style="padding:15px">testServerConnectivity.do</td>
    <td style="padding:15px">{base_path}/{version}/aaa/testServerConnectivity.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportLogsDo(startTime, endTime, category, objectKey, callback)</td>
    <td style="padding:15px">exportLogs.do</td>
    <td style="padding:15px">{base_path}/{version}/audit/exportLogs.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogsDo(body, callback)</td>
    <td style="padding:15px">getLogs.do</td>
    <td style="padding:15px">{base_path}/{version}/audit/getLogs.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNotesToChangeControlDo(body, callback)</td>
    <td style="padding:15px">addNotesToChangeControl.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/addNotesToChangeControl.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOrUpdateChangeControlDo(body, callback)</td>
    <td style="padding:15px">addOrUpdateChangeControl.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/addOrUpdateChangeControl.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelChangeControlDo(body, callback)</td>
    <td style="padding:15px">cancelChangeControl.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/cancelChangeControl.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneChangeControlDo(body, callback)</td>
    <td style="padding:15px">cloneChangeControl.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/cloneChangeControl.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteChangeControlsDo(body, callback)</td>
    <td style="padding:15px">deleteChangeControls.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/deleteChangeControls.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeCCDo(body, callback)</td>
    <td style="padding:15px">executeCC.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/executeCC.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCCProgressDo(body, callback)</td>
    <td style="padding:15px">getCCProgress.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/getCCProgress.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCcTasksByProgressStatusDo(body, callback)</td>
    <td style="padding:15px">getCcTasksByProgressStatus.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/getCcTasksByProgressStatus.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeControlInformationDo(searchText, startIndex, endIndex, ccId, callback)</td>
    <td style="padding:15px">getChangeControlInformation.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/getChangeControlInformation.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeControlsDo(searchText, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getChangeControls.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/getChangeControls.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventInfoDo(ccId, stage, requiredEventStatus, startIndex, endIndex, searchText, callback)</td>
    <td style="padding:15px">getEventInfo.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/getEventInfo.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasksByStatusDo(searchText, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getTasksByStatus.do</td>
    <td style="padding:15px">{base_path}/{version}/changeControl/getTasksByStatus.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConfigletDo(body, callback)</td>
    <td style="padding:15px">addConfiglet.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/addConfiglet.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConfigletBuilderDo(isDraft, body, callback)</td>
    <td style="padding:15px">addConfigletBuilder.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/addConfigletBuilder.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConfigletsAndAssociatedMappersDo(body, callback)</td>
    <td style="padding:15px">addConfigletsAndAssociatedMappers.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/addConfigletsAndAssociatedMappers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNoteToConfigletDo(body, callback)</td>
    <td style="padding:15px">addNoteToConfiglet.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/addNoteToConfiglet.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTempConfigletBuilderDo(type, id, body, callback)</td>
    <td style="padding:15px">addTempConfigletBuilder.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/addTempConfigletBuilder.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTempGeneratedConfigletDo(body, callback)</td>
    <td style="padding:15px">addTempGeneratedConfiglet.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/addTempGeneratedConfiglet.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">autoConfigletGeneratorDo(body, callback)</td>
    <td style="padding:15px">autoConfigletGenerator.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/autoConfigletGenerator.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelConfigletBuilderDo(id, callback)</td>
    <td style="padding:15px">cancelConfigletBuilder.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/cancelConfigletBuilder.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configletBuilderPreviewDo(body, callback)</td>
    <td style="padding:15px">configletBuilderPreview.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/configletBuilderPreview.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigletDo(body, callback)</td>
    <td style="padding:15px">deleteConfiglet.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/deleteConfiglet.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportConfigletsDo(body, callback)</td>
    <td style="padding:15px">exportConfiglets.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/exportConfiglets.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliedContainersDo(configletName, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getAppliedContainers.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getAppliedContainers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliedDeviceCountDo(configletId, callback)</td>
    <td style="padding:15px">getAppliedDeviceCount.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getAppliedDeviceCount.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliedDevicesDo(configletName, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getAppliedDevices.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getAppliedDevices.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletBuilderDo(id, type, callback)</td>
    <td style="padding:15px">getConfigletBuilder.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getConfigletBuilder.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletByIdDo(id, callback)</td>
    <td style="padding:15px">getConfigletById.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getConfigletById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletByNameDo(name, callback)</td>
    <td style="padding:15px">getConfigletByName.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getConfigletByName.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletHistoryDo(configletId, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getConfigletHistory.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getConfigletHistory.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletsDo(objectType, objectId, type, startIndex, endIndex, sortByColumn, sortOrder, callback)</td>
    <td style="padding:15px">getConfiglets.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getConfiglets.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletsAndAssociatedMappersDo(callback)</td>
    <td style="padding:15px">getConfigletsAndAssociatedMappers.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getConfigletsAndAssociatedMappers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentManagementIpDo(body, callback)</td>
    <td style="padding:15px">getCurrentManagementIp.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getCurrentManagementIp.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHierarchicalBuilderCountDo(containerId, callback)</td>
    <td style="padding:15px">getHierarchicalBuilderCount.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getHierarchicalBuilderCount.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHierarchicalConfigletBuildersDo(containerId, queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getHierarchicalConfigletBuilders.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getHierarchicalConfigletBuilders.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementIpDo(queryparam, startIndex, endIndex, body, callback)</td>
    <td style="padding:15px">getManagementIp.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/getManagementIp.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importConfigletsDo(file, callback)</td>
    <td style="padding:15px">importConfiglets.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/importConfiglets.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchConfigletsDo(objectType, objectId, type, queryparam, startIndex, endIndex, sortByColumn, sortOrder, callback)</td>
    <td style="padding:15px">searchConfiglets.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/searchConfiglets.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfigletDo(body, callback)</td>
    <td style="padding:15px">updateConfiglet.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/updateConfiglet.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfigletBuilderDo(isDraft, id, action, body, callback)</td>
    <td style="padding:15px">updateConfigletBuilder.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/updateConfigletBuilder.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateConfigDo(body, callback)</td>
    <td style="padding:15px">validateConfig.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/validateConfig.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateConfigHistoryDo(historyId, callback)</td>
    <td style="padding:15px">validateConfigHistory.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/validateConfigHistory.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateScriptDo(body, callback)</td>
    <td style="padding:15px">validateScript.do</td>
    <td style="padding:15px">{base_path}/{version}/configlet/validateScript.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCvpInfoDo(callback)</td>
    <td style="padding:15px">getCvpInfo.do</td>
    <td style="padding:15px">{base_path}/{version}/cvpInfo/getCvpInfo.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelEventDo(eventId, callback)</td>
    <td style="padding:15px">cancelEvent.do</td>
    <td style="padding:15px">{base_path}/{version}/event/cancelEvent.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllEventsDo(startIndex, endIndex, isCompletedRequired, callback)</td>
    <td style="padding:15px">getAllEvents.do</td>
    <td style="padding:15px">{base_path}/{version}/event/getAllEvents.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getErrorEventsDo(queryparam, eventId, eventType, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getErrorEvents.do</td>
    <td style="padding:15px">{base_path}/{version}/event/getErrorEvents.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventByIdDo(eventId, callback)</td>
    <td style="padding:15px">getEventById.do</td>
    <td style="padding:15px">{base_path}/{version}/event/getEventById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventDataByIdDo(eventId, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getEventDataById.do</td>
    <td style="padding:15px">{base_path}/{version}/event/getEventDataById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markAsReadDo(callback)</td>
    <td style="padding:15px">markAsRead.do</td>
    <td style="padding:15px">{base_path}/{version}/event/markAsRead.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addImageDo(file, callback)</td>
    <td style="padding:15px">addImage.do</td>
    <td style="padding:15px">{base_path}/{version}/image/addImage.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNotesToImageBundleDo(body, callback)</td>
    <td style="padding:15px">addNotesToImageBundle.do</td>
    <td style="padding:15px">{base_path}/{version}/image/addNotesToImageBundle.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelImagesDo(body, callback)</td>
    <td style="padding:15px">cancelImages.do</td>
    <td style="padding:15px">{base_path}/{version}/image/cancelImages.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImageBundlesDo(body, callback)</td>
    <td style="padding:15px">deleteImageBundles.do</td>
    <td style="padding:15px">{base_path}/{version}/image/deleteImageBundles.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundleAppliedContainersDo(imageName, startIndex, endIndex, queryparam, callback)</td>
    <td style="padding:15px">getImageBundleAppliedContainers.do</td>
    <td style="padding:15px">{base_path}/{version}/image/getImageBundleAppliedContainers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundleAppliedDevicesDo(imageName, startIndex, endIndex, queryparam, callback)</td>
    <td style="padding:15px">getImageBundleAppliedDevices.do</td>
    <td style="padding:15px">{base_path}/{version}/image/getImageBundleAppliedDevices.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundleByNameDo(name, callback)</td>
    <td style="padding:15px">getImageBundleByName.do</td>
    <td style="padding:15px">{base_path}/{version}/image/getImageBundleByName.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundlesDo(objectId, objectType, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getImageBundles.do</td>
    <td style="padding:15px">{base_path}/{version}/image/getImageBundles.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesDo(queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getImages.do</td>
    <td style="padding:15px">{base_path}/{version}/image/getImages.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveImageBundleDo(body, callback)</td>
    <td style="padding:15px">saveImageBundle.do</td>
    <td style="padding:15px">{base_path}/{version}/image/saveImageBundle.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateImageBundleDo(body, callback)</td>
    <td style="padding:15px">updateImageBundle.do</td>
    <td style="padding:15px">{base_path}/{version}/image/updateImageBundle.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundleByNameV2Do(name, callback)</td>
    <td style="padding:15px">getImageBundleByNameV2.do</td>
    <td style="padding:15px">{base_path}/{version}/image/v2/getImageBundleByName.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundlesV2Do(objectId, objectType, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getImageBundlesV2.do</td>
    <td style="padding:15px">{base_path}/{version}/image/v2/getImageBundles.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainers(name, callback)</td>
    <td style="padding:15px">getContainers</td>
    <td style="padding:15px">{base_path}/{version}/inventory/containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConfiguration(netElementId, callback)</td>
    <td style="padding:15px">GetDeviceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/inventory/device/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(body, callback)</td>
    <td style="padding:15px">deleteDevice</td>
    <td style="padding:15px">{base_path}/{version}/inventory/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(provisioned, callback)</td>
    <td style="padding:15px">getDevices</td>
    <td style="padding:15px">{base_path}/{version}/inventory/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">onboardDevice(body, callback)</td>
    <td style="padding:15px">onboardDevice</td>
    <td style="padding:15px">{base_path}/{version}/inventory/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLabelDo(body, callback)</td>
    <td style="padding:15px">addLabel.do</td>
    <td style="padding:15px">{base_path}/{version}/label/addLabel.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLabelDo(body, callback)</td>
    <td style="padding:15px">deleteLabel.do</td>
    <td style="padding:15px">{base_path}/{version}/label/deleteLabel.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabelInfoDo(labelId, callback)</td>
    <td style="padding:15px">getLabelInfo.do</td>
    <td style="padding:15px">{base_path}/{version}/label/getLabelInfo.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabelsDo(module, type, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getLabels.do</td>
    <td style="padding:15px">{base_path}/{version}/label/getLabels.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLabelDo(body, callback)</td>
    <td style="padding:15px">updateLabel.do</td>
    <td style="padding:15px">{base_path}/{version}/label/updateLabel.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNotesToLabelDo(body, callback)</td>
    <td style="padding:15px">updateNotesToLabel.do</td>
    <td style="padding:15px">{base_path}/{version}/label/updateNotesToLabel.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticateDo(body, callback)</td>
    <td style="padding:15px">authenticate.do</td>
    <td style="padding:15px">{base_path}/{version}/login/authenticate.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePasswordDo(body, callback)</td>
    <td style="padding:15px">changePassword.do</td>
    <td style="padding:15px">{base_path}/{version}/login/changePassword.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCVPUserProfileDo(userId, callback)</td>
    <td style="padding:15px">getCVPUserProfile.do</td>
    <td style="padding:15px">{base_path}/{version}/login/getCVPUserProfile.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsDo(callback)</td>
    <td style="padding:15px">getPermissions.do</td>
    <td style="padding:15px">{base_path}/{version}/login/getPermissions.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">homeDo(callback)</td>
    <td style="padding:15px">home.do</td>
    <td style="padding:15px">{base_path}/{version}/login/home.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutDo(callback)</td>
    <td style="padding:15px">logout.do</td>
    <td style="padding:15px">{base_path}/{version}/login/logout.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkRollbackTempActionsDo(body, callback)</td>
    <td style="padding:15px">addNetworkRollbackTempActions.do</td>
    <td style="padding:15px">{base_path}/{version}/rollback/addNetworkRollbackTempActions.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTempRollbackActionDo(body, callback)</td>
    <td style="padding:15px">addTempRollbackAction.do</td>
    <td style="padding:15px">{base_path}/{version}/rollback/addTempRollbackAction.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHierarchyChangeInfoDo(containerId, timestamp, searchText, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getHierarchyChangeInfo.do</td>
    <td style="padding:15px">{base_path}/{version}/rollback/getNetElementsForRollback.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceConfigs(deviceID, current, timestamp, callback)</td>
    <td style="padding:15px">deviceConfigs</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/deviceConfigs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnapshotChartDataDo(body, callback)</td>
    <td style="padding:15px">getSnapshotChartData.do</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/getSnapshotChartData.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hierarchychangeinfo(timestamp, body, callback)</td>
    <td style="padding:15px">hierarchychangeinfo</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/hierarchychangeinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSnapshotChartDataDo(templateId, callback)</td>
    <td style="padding:15px">getSnapshotChartData.do</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templates(body, callback)</td>
    <td style="padding:15px">templates</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gettemplates(queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">templates</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">templatesinfo(body, callback)</td>
    <td style="padding:15px">templatesinfo</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/templates/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">snapshotTemplatesSchedule(body, callback)</td>
    <td style="padding:15px">/snapshot/templates/schedule</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/templates/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">capture(templateKey, body, callback)</td>
    <td style="padding:15px">capture</td>
    <td style="padding:15px">{base_path}/{version}/snapshot/templates/{pathv1}/capture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bindCertWithCSRDo(body, callback)</td>
    <td style="padding:15px">bindCertWithCSR.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/bindCertWithCSR.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCSRDo(callback)</td>
    <td style="padding:15px">deleteCSR.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/deleteCSR.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProposedCertificateDo(callback)</td>
    <td style="padding:15px">deleteProposedCertificate.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/deleteProposedCertificate.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportCSRDo(callback)</td>
    <td style="padding:15px">exportCSR.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/exportCSR.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportCertificateDo(body, callback)</td>
    <td style="padding:15px">exportCertificate.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/exportCertificate.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCSRDo(body, callback)</td>
    <td style="padding:15px">generateCSR.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/generateCSR.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCertificateDo(body, callback)</td>
    <td style="padding:15px">generateCertificate.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/generateCertificate.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateDo(certType, callback)</td>
    <td style="padding:15px">getCertificate.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/getCertificate.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCertAndPrivateKeyDo(body, callback)</td>
    <td style="padding:15px">importCertAndPrivateKey.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/importCertAndPrivateKey.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installCertificateDo(callback)</td>
    <td style="padding:15px">installCertificate.do</td>
    <td style="padding:15px">{base_path}/{version}/ssl/installCertificate.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeTaskDo(body, callback)</td>
    <td style="padding:15px">executeTask.do</td>
    <td style="padding:15px">{base_path}/{version}/task/addNoteToTask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelTaskDo(body, callback)</td>
    <td style="padding:15px">cancelTask.do</td>
    <td style="padding:15px">{base_path}/{version}/task/cancelTask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postexecuteTaskDo(body, callback)</td>
    <td style="padding:15px">executeTask.do</td>
    <td style="padding:15px">{base_path}/{version}/task/executeTask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskByIdDo(taskId, callback)</td>
    <td style="padding:15px">getTaskById.do</td>
    <td style="padding:15px">{base_path}/{version}/task/getTaskById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskStatusByIdDo(taskId, callback)</td>
    <td style="padding:15px">getTaskStatusById.do</td>
    <td style="padding:15px">{base_path}/{version}/task/getTaskStatusById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasksDo(queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getTasks.do</td>
    <td style="padding:15px">{base_path}/{version}/task/getTasks.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWorkOrderLogDo(body, callback)</td>
    <td style="padding:15px">addWorkOrderLog.do</td>
    <td style="padding:15px">{base_path}/{version}/workflow/addWorkOrderLog.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trustedCertificatesDeleteDo(body, callback)</td>
    <td style="padding:15px">/trustedCertificates/delete.do</td>
    <td style="padding:15px">{base_path}/{version}/trustedCertificates/delete.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trustedCertificatesExportDo(body, callback)</td>
    <td style="padding:15px">/trustedCertificates/export.do</td>
    <td style="padding:15px">{base_path}/{version}/trustedCertificates/export.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trustedCertificatesGetCertInfoDo(fingerPrint, callback)</td>
    <td style="padding:15px">/trustedCertificates/getCertInfo.do</td>
    <td style="padding:15px">{base_path}/{version}/trustedCertificates/getCertInfo.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trustedCertificatesGetCertsDo(queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">/trustedCertificates/getCerts.do</td>
    <td style="padding:15px">{base_path}/{version}/trustedCertificates/getCerts.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trustedCertificatesUploadDo(body, callback)</td>
    <td style="padding:15px">/trustedCertificates/upload.do</td>
    <td style="padding:15px">{base_path}/{version}/trustedCertificates/upload.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserDo(body, callback)</td>
    <td style="padding:15px">addUser.do</td>
    <td style="padding:15px">{base_path}/{version}/user/addUser.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersDo(body, callback)</td>
    <td style="padding:15px">deleteUsers.do</td>
    <td style="padding:15px">{base_path}/{version}/user/deleteUsers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDisableUsersDo(state, body, callback)</td>
    <td style="padding:15px">enableDisableUsers.do</td>
    <td style="padding:15px">{base_path}/{version}/user/enableDisableUsers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnlineUserCountDo(callback)</td>
    <td style="padding:15px">getOnlineUserCount.do</td>
    <td style="padding:15px">{base_path}/{version}/user/getOnlineUserCount.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDo(userId, callback)</td>
    <td style="padding:15px">getUser.do</td>
    <td style="padding:15px">{base_path}/{version}/user/getUser.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersDo(queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getUsers.do</td>
    <td style="padding:15px">{base_path}/{version}/user/getUsers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserDo(userId, body, callback)</td>
    <td style="padding:15px">updateUser.do</td>
    <td style="padding:15px">{base_path}/{version}/user/updateUser.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRoleDo(body, callback)</td>
    <td style="padding:15px">createRole.do</td>
    <td style="padding:15px">{base_path}/{version}/role/createRole.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliedUserCountDo(roleId, callback)</td>
    <td style="padding:15px">getAppliedUserCount.do</td>
    <td style="padding:15px">{base_path}/{version}/role/getAppliedUserCount.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliedUsersDo(roleName, queryparam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getAppliedUsers.do</td>
    <td style="padding:15px">{base_path}/{version}/role/getAppliedUsers.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleDo(roleId, callback)</td>
    <td style="padding:15px">getRole.do</td>
    <td style="padding:15px">{base_path}/{version}/role/getRole.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRolesDo(queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getRoles.do</td>
    <td style="padding:15px">{base_path}/{version}/role/getRoles.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoleDo(body, callback)</td>
    <td style="padding:15px">updateRole.do</td>
    <td style="padding:15px">{base_path}/{version}/role/updateRole.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRolesDo(body, callback)</td>
    <td style="padding:15px">deleteRoles.do</td>
    <td style="padding:15px">{base_path}/{version}/role/deleteRoles.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTempActionDo(nodeId, queryParam, format, body, callback)</td>
    <td style="padding:15px">addTempAction.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/addTempAction.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelReconcileDo(configletId, callback)</td>
    <td style="padding:15px">cancelReconcile.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/cancelReconcile.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionStreamingDevices(body, callback)</td>
    <td style="padding:15px">provisionStreamingDevices</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkComplianceDo(body, callback)</td>
    <td style="padding:15px">checkCompliance.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/checkCompliance.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">containerLevelReconcileDo(containerId, reconcileAll, callback)</td>
    <td style="padding:15px">containerLevelReconcile.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/containerLevelReconcile.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllTempActionDo(callback)</td>
    <td style="padding:15px">deleteAllTempAction.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/deleteAllTempAction.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postdeleteAllTempActionDo(body, callback)</td>
    <td style="padding:15px">deleteAllTempAction.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/deleteAllTempAction.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTempActionDo(body, callback)</td>
    <td style="padding:15px">deleteTempAction.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/deleteTempAction.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterTopologyDo(nodeId, queryParam, format, startIndex, endIndex, callback)</td>
    <td style="padding:15px">filterTopology.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/filterTopology.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTempActionsDo(startIndex, endIndex, callback)</td>
    <td style="padding:15px">getAllTempActions.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getAllTempActions.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletsByContainerIdDo(containerId, queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getConfigletsByContainerId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getConfigletsByContainerId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigletsByNetElementIdDo(netElementId, queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getConfigletsByNetElementId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getConfigletsByNetElementId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">topologyDo(containerId, callback)</td>
    <td style="padding:15px">topology.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getContainerInfoById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundleByContainerIdDo(containerId, sessionScope, queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getImageBundleByContainerId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getImageBundleByContainerId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageBundleByNetElementIdDo(netElementId, sessionScope, queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">getImageBundleByNetElementId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getImageBundleByNetElementId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageForTaskDo(workorderid, callback)</td>
    <td style="padding:15px">getImageForTask.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getImageForTask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabelsByNetElementIdDo(queryParam, startIndex, endIndex, netElementId, type, callback)</td>
    <td style="padding:15px">getLabelsByNetElementId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getLabelsByNetElementId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetElementByIdDo(netElementId, callback)</td>
    <td style="padding:15px">getNetElementById.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getNetElementById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetElementImageByIdDo(netElementId, callback)</td>
    <td style="padding:15px">getNetElementImageById.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getNetElementImageById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetElementInfoByIdDo(netElementId, callback)</td>
    <td style="padding:15px">getNetElementInfoById.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getNetElementInfoById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetElementListDo(nodeId, queryParam, startIndex, endIndex, contextQueryParam, ignoreAdd, callback)</td>
    <td style="padding:15px">getNetElementList.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getNetElementList.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProposedContainerDo(netElementId, callback)</td>
    <td style="padding:15px">getProposedContainer.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getProposedContainer.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTempConfigsByContainerIdDo(containerId, pageType, callback)</td>
    <td style="padding:15px">getTempConfigsByContainerId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getTempConfigsByContainerId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTempConfigsByNetElementIdDo(netElementId, callback)</td>
    <td style="padding:15px">getTempConfigsByNetElementId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getTempConfigsByNetElementId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTempLabelsByNetElementIdDo(netElementId, callback)</td>
    <td style="padding:15px">getTempLabelsByNetElementId.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getTempLabelsByNetElementId.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTempSessionReconciledConfigletsDo(netElementId, callback)</td>
    <td style="padding:15px">getTempSessionReconciledConfiglets.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getTempSessionReconciledConfiglets.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getconfigfortaskDo(workorderid, callback)</td>
    <td style="padding:15px">getconfigfortask.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/getconfigfortask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipConnectivityTestDo(body, callback)</td>
    <td style="padding:15px">ipConnectivityTest.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/ipConnectivityTest.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveTopologyDo(body, callback)</td>
    <td style="padding:15px">saveTopology.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/saveTopology.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTopologyDo(queryParam, startIndex, endIndex, callback)</td>
    <td style="padding:15px">searchTopology.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/searchTopology.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetElementImageByIdDo(netElementId, netElementImage, callback)</td>
    <td style="padding:15px">updateNetElementImageById.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/updateNetElementImageById.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReconcileConfigletDo(netElementId, body, callback)</td>
    <td style="padding:15px">updateReconcileConfiglet.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/updateReconcileConfiglet.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">v2GetconfigfortaskDo(workorderid, callback)</td>
    <td style="padding:15px">v2/getconfigfortask.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/v2/getconfigfortask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveTopologyDo1(body, callback)</td>
    <td style="padding:15px">saveTopology.do1</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/v2/saveTopology.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateAndCompareConfigletsDo(body, callback)</td>
    <td style="padding:15px">validateAndCompareConfiglets.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/v2/validateAndCompareConfiglets.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">v3GetconfigfortaskDo(workorderid, configviewmode, callback)</td>
    <td style="padding:15px">v3/getconfigfortask.do</td>
    <td style="padding:15px">{base_path}/{version}/provisioning/v3/getconfigfortask.do?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
