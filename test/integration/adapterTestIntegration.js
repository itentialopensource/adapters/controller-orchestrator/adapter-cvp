/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cvp',
      type: 'Cvp',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Cvp = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Cvp Adapter Test', () => {
  describe('Cvp Class Tests', () => {
    const a = new Cvp(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const aaaCreateServerDoBodyParam = {
      accountPort: 8,
      authMode: 'string',
      createdDateInLongFormat: 3,
      ipAddress: 'string',
      port: 1,
      secret: 'string',
      serverType: 'string',
      status: 'string'
    };
    describe('#createServerDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createServerDo(aaaCreateServerDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'createServerDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aaaDeleteServerDoBodyParam = {};
    describe('#deleteServerDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteServerDo(aaaDeleteServerDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'deleteServerDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aaaEditServerDoBodyParam = {
      accountPort: 7,
      authMode: 'string',
      createdDateInLongFormat: 1,
      ipAddress: 'string',
      key: 'string',
      port: 6,
      secret: 'string',
      serverType: 'string',
      status: 'string'
    };
    describe('#editServerDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editServerDo(aaaEditServerDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'editServerDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aaaSaveAAADetailsDoBodyParam = {
      authenticationServerType: 'string',
      authorizationServerType: 'string'
    };
    describe('#saveAAADetailsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saveAAADetailsDo(aaaSaveAAADetailsDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'saveAAADetailsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aaaTestServerConnectivityDoBodyParam = {
      server: {
        accountPort: 4,
        authMode: 'string',
        ipAddress: 'string',
        port: 7,
        secret: 'string',
        serverType: 'string',
        status: 'string'
      },
      user: {
        password: 'string',
        userId: 'string'
      }
    };
    describe('#testServerConnectivityDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.testServerConnectivityDo(aaaTestServerConnectivityDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'testServerConnectivityDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAADetailsByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAAADetailsByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.authenticationServerType);
                assert.equal('string', data.response.authorizationServerType);
                assert.equal(2, data.response.factoryId);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'getAAADetailsByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let aaaServerType = 'fakedata';
    describe('#getServerByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServerByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.accountPort);
                assert.equal('string', data.response.authMode);
                assert.equal(2, data.response.createdDateInLongFormat);
                assert.equal(7, data.response.factoryId);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.liveStatus);
                assert.equal(4, data.response.port);
                assert.equal('string', data.response.secret);
                assert.equal('string', data.response.serverType);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              aaaServerType = data.response.serverType;
              saveMockData('Aaa', 'getServerByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aaaStartIndex = 555;
    const aaaEndIndex = 555;
    describe('#getServersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServersDo(aaaServerType, null, aaaStartIndex, aaaEndIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.aaaServers));
                assert.equal(3, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aaa', 'getServersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportLogsDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportLogsDo('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'exportLogsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditGetLogsDoBodyParam = {
      category: 'string',
      dataSize: 8,
      endTime: 4,
      lastRetrievedAudit: {
        category: 'string',
        objectKey: 'string'
      },
      objectKey: 'string',
      queryParam: 'string',
      startTime: 4
    };
    describe('#getLogsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLogsDo(auditGetLogsDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'getLogsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlAddNotesToChangeControlDoBodyParam = {
      ccId: 'string',
      notes: 'string'
    };
    describe('#addNotesToChangeControlDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNotesToChangeControlDo(changeControlAddNotesToChangeControlDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'addNotesToChangeControlDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlAddOrUpdateChangeControlDoBodyParam = {
      ccName: 'string',
      snapshotTemplateKey: 'string',
      stopOnError: {}
    };
    describe('#addOrUpdateChangeControlDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addOrUpdateChangeControlDo(changeControlAddOrUpdateChangeControlDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ccId);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'addOrUpdateChangeControlDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlCancelChangeControlDoBodyParam = {
      ccIds: [
        'string'
      ]
    };
    describe('#cancelChangeControlDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelChangeControlDo(changeControlCancelChangeControlDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'cancelChangeControlDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlCloneChangeControlDoBodyParam = {
      ccIds: [
        'string'
      ]
    };
    describe('#cloneChangeControlDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloneChangeControlDo(changeControlCloneChangeControlDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ccName);
                assert.equal('object', typeof data.response.changeControlTasks);
                assert.equal('string', data.response.snapshotTemplateKey);
                assert.equal('string', data.response.snapshotTemplateName);
                assert.equal(true, data.response.stopOnError);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'cloneChangeControlDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlDeleteChangeControlsDoBodyParam = {
      ccIds: [
        'string'
      ]
    };
    describe('#deleteChangeControlsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteChangeControlsDo(changeControlDeleteChangeControlsDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'deleteChangeControlsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlExecuteCCDoBodyParam = {
      ccIds: [
        {}
      ]
    };
    describe('#executeCCDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeCCDo(changeControlExecuteCCDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'executeCCDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlGetCCProgressDoBodyParam = {
      ccId: 'string',
      taskList: [
        'string'
      ]
    };
    describe('#getCCProgressDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCCProgressDo(changeControlGetCCProgressDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.changeControlTasks));
                assert.equal('object', typeof data.response.progressStatus);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'getCCProgressDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeControlGetCcTasksByProgressStatusDoBodyParam = {
      ccId: 'string',
      endIndex: 8,
      postSnapshotStatus: 'string',
      preSnapshotStatus: 'string',
      searchText: 'string',
      startIndex: 5,
      taskStatus: 'string'
    };
    describe('#getCcTasksByProgressStatusDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCcTasksByProgressStatusDo(changeControlGetCcTasksByProgressStatusDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(6, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'getCcTasksByProgressStatusDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChangeControlInformationDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChangeControlInformationDo(null, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ccId);
                assert.equal('string', data.response.ccName);
                assert.equal('object', typeof data.response.changeControlTasks);
                assert.equal(1, data.response.classId);
                assert.equal('string', data.response.containerName);
                assert.equal('string', data.response.countryId);
                assert.equal('string', data.response.createdBy);
                assert.equal(1, data.response.createdTimestamp);
                assert.equal('string', data.response.dateTime);
                assert.equal(9, data.response.deviceCount);
                assert.equal('string', data.response.executedBy);
                assert.equal(6, data.response.executedTimestamp);
                assert.equal(4, data.response.factoryId);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.notes);
                assert.equal(8, data.response.postSnapshotEndTime);
                assert.equal(4, data.response.postSnapshotStartTime);
                assert.equal(10, data.response.preSnapshotEndTime);
                assert.equal(2, data.response.preSnapshotStartTime);
                assert.equal('object', typeof data.response.progressStatus);
                assert.equal('string', data.response.scheduledBy);
                assert.equal('string', data.response.scheduledByPassword);
                assert.equal(3, data.response.scheduledTimestamp);
                assert.equal('string', data.response.snapshotTemplateKey);
                assert.equal('string', data.response.snapshotTemplateName);
                assert.equal('object', typeof data.response.status);
                assert.equal(true, data.response.stopOnError);
                assert.equal(9, data.response.taskCount);
                assert.equal(9, data.response.taskEndTime);
                assert.equal(3, data.response.taskStartTime);
                assert.equal('string', data.response.timeZone);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'getChangeControlInformationDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChangeControlsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChangeControlsDo(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(5, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'getChangeControlsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventInfoDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventInfoDo('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(2, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'getEventInfoDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasksByStatusDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasksByStatusDo(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(8, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeControl', 'getTasksByStatusDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAddConfigletDoBodyParam = {
      config: 'string',
      name: 'string'
    };
    describe('#addConfigletDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addConfigletDo(configletAddConfigletDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'addConfigletDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAddConfigletBuilderDoBodyParam = {
      data: {},
      name: 'string'
    };
    describe('#addConfigletBuilderDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addConfigletBuilderDo('fakedata', configletAddConfigletBuilderDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'addConfigletBuilderDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAddConfigletsAndAssociatedMappersDoBodyParam = {
      data: {}
    };
    describe('#addConfigletsAndAssociatedMappersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addConfigletsAndAssociatedMappersDo(configletAddConfigletsAndAssociatedMappersDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'addConfigletsAndAssociatedMappersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAddNoteToConfigletDoBodyParam = {
      key: 'string',
      note: 'string'
    };
    describe('#addNoteToConfigletDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNoteToConfigletDo(configletAddNoteToConfigletDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'addNoteToConfigletDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAddTempConfigletBuilderDoBodyParam = {
      action: 'string',
      formList: [
        {}
      ],
      mainScript: {}
    };
    describe('#addTempConfigletBuilderDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTempConfigletBuilderDo(null, null, configletAddTempConfigletBuilderDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'addTempConfigletBuilderDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAddTempGeneratedConfigletDoBodyParam = {
      data: [
        {}
      ]
    };
    describe('#addTempGeneratedConfigletDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTempGeneratedConfigletDo(configletAddTempGeneratedConfigletDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.action);
                assert.equal('string', data.response.configletBuilderId);
                assert.equal('string', data.response.configletId);
                assert.equal('string', data.response.containerId);
                assert.equal(9, data.response.factoryId);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.netElementId);
                assert.equal('string', data.response.objectType);
                assert.equal(true, Array.isArray(data.response.previewValues));
                assert.equal(8, data.response.previewValuesListSize);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'addTempGeneratedConfigletDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletAutoConfigletGeneratorDoBodyParam = {
      configletBuilderId: 'string',
      containerId: 'string',
      netElementIds: [
        'string'
      ],
      pageType: 'string'
    };
    describe('#autoConfigletGeneratorDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.autoConfigletGeneratorDo(configletAutoConfigletGeneratorDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'autoConfigletGeneratorDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelConfigletBuilderDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelConfigletBuilderDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'cancelConfigletBuilderDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletConfigletBuilderPreviewDoBodyParam = {
      configletBuilderId: 'string',
      containerId: 'string',
      containerToId: 'string',
      mode: 'string',
      pageType: 'string',
      previewValues: [
        {}
      ]
    };
    describe('#configletBuilderPreviewDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.configletBuilderPreviewDo(configletConfigletBuilderPreviewDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'configletBuilderPreviewDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletDeleteConfigletDoBodyParam = {
      key: 'string',
      name: 'string'
    };
    describe('#deleteConfigletDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConfigletDo(configletDeleteConfigletDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'deleteConfigletDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletExportConfigletsDoBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#exportConfigletsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportConfigletsDo(configletExportConfigletsDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'exportConfigletsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliedContainersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliedContainersDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(6, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getAppliedContainersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliedDeviceCountDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliedDeviceCountDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getAppliedDeviceCountDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliedDevicesDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliedDevicesDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(2, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getAppliedDevicesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletBuilderDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletBuilderDo('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getConfigletBuilderDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.config);
                assert.equal(9, data.response.containerCount);
                assert.equal(6, data.response.dateTimeInLongFormat);
                assert.equal(1, data.response.factoryId);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.isAutoBuilder);
                assert.equal('string', data.response.isDefault);
                assert.equal(true, data.response.isDraft);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.netElementCount);
                assert.equal('string', data.response.note);
                assert.equal(true, data.response.reconciled);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getConfigletByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletByNameDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletByNameDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.config);
                assert.equal(10, data.response.containerCount);
                assert.equal(2, data.response.dateTimeInLongFormat);
                assert.equal(10, data.response.factoryId);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.isAutoBuilder);
                assert.equal('string', data.response.isDefault);
                assert.equal(false, data.response.isDraft);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.netElementCount);
                assert.equal('string', data.response.note);
                assert.equal(false, data.response.reconciled);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getConfigletByNameDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletHistoryDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletHistoryDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.configletHistory));
                assert.equal(7, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getConfigletHistoryDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletsDo(null, null, null, 555, 555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(5, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getConfigletsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletsAndAssociatedMappersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletsAndAssociatedMappersDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getConfigletsAndAssociatedMappersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletGetCurrentManagementIpDoBodyParam = {
      configIdList: [
        'string'
      ],
      netElementId: 'string'
    };
    describe('#getCurrentManagementIpDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentManagementIpDo(configletGetCurrentManagementIpDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getCurrentManagementIpDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHierarchicalBuilderCountDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHierarchicalBuilderCountDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getHierarchicalBuilderCountDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHierarchicalConfigletBuildersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHierarchicalConfigletBuildersDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.buildMapperList));
                assert.equal(true, Array.isArray(data.response.builderList));
                assert.equal(5, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getHierarchicalConfigletBuildersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletGetManagementIpDoBodyParam = {
      configIdList: [
        'string'
      ],
      netElementId: 'string',
      pageType: 'string'
    };
    describe('#getManagementIpDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManagementIpDo(null, 555, 555, configletGetManagementIpDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.defaultProposedManagementIp);
                assert.equal(true, Array.isArray(data.response.managementIps));
                assert.equal(10, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'getManagementIpDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importConfigletsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importConfigletsDo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
                assert.equal('string', data.response.eventId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'importConfigletsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchConfigletsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchConfigletsDo(null, null, null, null, 555, 555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(7, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'searchConfigletsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletUpdateConfigletDoBodyParam = {
      config: 'string',
      key: 'string',
      name: 'string'
    };
    describe('#updateConfigletDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConfigletDo(configletUpdateConfigletDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'updateConfigletDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletUpdateConfigletBuilderDoBodyParam = {
      data: {},
      name: 'string'
    };
    describe('#updateConfigletBuilderDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConfigletBuilderDo('fakedata', 'fakedata', null, configletUpdateConfigletBuilderDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
                assert.equal('string', data.response.eventId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'updateConfigletBuilderDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletValidateConfigDoBodyParam = {
      config: 'string',
      netElementId: 'string'
    };
    describe('#validateConfigDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateConfigDo(configletValidateConfigDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.errorCount);
                assert.equal(true, Array.isArray(data.response.errors));
                assert.equal(9, data.response.warningCount);
                assert.equal(true, Array.isArray(data.response.warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'validateConfigDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateConfigHistoryDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateConfigHistoryDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.deleted);
                assert.equal(5, data.response.mismatch);
                assert.equal(2, data.response.new);
                assert.equal(true, Array.isArray(data.response.newConfig));
                assert.equal(true, Array.isArray(data.response.oldConfig));
                assert.equal(9, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'validateConfigHistoryDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configletValidateScriptDoBodyParam = {
      data: 'string'
    };
    describe('#validateScriptDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateScriptDo(configletValidateScriptDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.result);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configlet', 'validateScriptDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCvpInfoDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCvpInfoDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CvpInfo', 'getCvpInfoDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelEventDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelEventDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'cancelEventDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllEventsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllEventsDo(555, 555, true, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.externalEvents);
                assert.equal(9, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'getAllEventsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getErrorEventsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getErrorEventsDo(null, 'fakedata', 'fakedata', 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
                assert.equal(6, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'getErrorEventsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'getEventByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventDataByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventDataByIdDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(5, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'getEventDataByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#markAsReadDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.markAsReadDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Event', 'markAsReadDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addImageDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addImageDo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.imageId);
                assert.equal('string', data.response.imageSize);
                assert.equal('string', data.response.isRebootRequired);
                assert.equal('string', data.response.md5);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.result);
                assert.equal('string', data.response.sha512);
                assert.equal('string', data.response.swiMaxHwepoch);
                assert.equal('string', data.response.swiVarient);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'addImageDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageAddNotesToImageBundleDoBodyParam = {
      name: 'string',
      note: 'string'
    };
    describe('#addNotesToImageBundleDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNotesToImageBundleDo(imageAddNotesToImageBundleDoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'addNotesToImageBundleDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageCancelImagesDoBodyParam = {
      data: 'string'
    };
    describe('#cancelImagesDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelImagesDo(imageCancelImagesDoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'cancelImagesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageDeleteImageBundlesDoBodyParam = {
      data: [
        {}
      ]
    };
    describe('#deleteImageBundlesDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteImageBundlesDo(imageDeleteImageBundlesDoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'deleteImageBundlesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundleAppliedContainersDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageBundleAppliedContainersDo('fakedata', 555, 555, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImageBundleAppliedContainersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundleAppliedDevicesDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageBundleAppliedDevicesDo('fakedata', 555, 555, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImageBundleAppliedDevicesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundleByNameDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageBundleByNameDo('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImageBundleByNameDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundlesDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageBundlesDo(null, null, null, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImageBundlesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagesDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImagesDo(null, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImagesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageSaveImageBundleDoBodyParam = {
      images: [
        {}
      ],
      isCertifiedImage: 'string',
      name: 'string'
    };
    describe('#saveImageBundleDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveImageBundleDo(imageSaveImageBundleDoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'saveImageBundleDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const imageUpdateImageBundleDoBodyParam = {
      deletedExtensions: [
        {}
      ],
      id: 'string',
      images: [
        {}
      ],
      isCertifiedImage: 'string',
      name: 'string'
    };
    describe('#updateImageBundleDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateImageBundleDo(imageUpdateImageBundleDoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'updateImageBundleDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundleByNameV2Do - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageBundleByNameV2Do('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImageBundleByNameV2Do', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundlesV2Do - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageBundlesV2Do(null, null, null, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Image', 'getImageBundlesV2Do', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContainers(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceConfiguration('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.output);
                assert.equal(true, Array.isArray(data.response.warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getDeviceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryDeleteDeviceBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#deleteDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDevice(inventoryDeleteDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'deleteDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevices(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryOnboardDeviceBodyParam = {
      hosts: [
        'string'
      ]
    };
    describe('#onboardDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.onboardDevice(inventoryOnboardDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.failed));
                assert.equal('object', typeof data.response.hostToDevIdMap);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'onboardDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelAddLabelDoBodyParam = {
      name: 'string',
      note: 'string',
      type: 'string'
    };
    describe('#addLabelDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addLabelDo(labelAddLabelDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.dateTimeInLongFormat);
                assert.equal(8, data.response.factoryId);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.netElementCount);
                assert.equal('string', data.response.note);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Label', 'addLabelDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelDeleteLabelDoBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#deleteLabelDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLabelDo(labelDeleteLabelDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Label', 'deleteLabelDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabelInfoDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLabelInfoDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.dateTimeInLongFormat);
                assert.equal(2, data.response.factoryId);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.netElementCount);
                assert.equal('string', data.response.note);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Label', 'getLabelInfoDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabelsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLabelsDo('fakedata', 'fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.labels));
                assert.equal(5, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Label', 'getLabelsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelUpdateLabelDoBodyParam = {
      key: 'string',
      name: 'string',
      note: 'string',
      type: 'string'
    };
    describe('#updateLabelDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateLabelDo(labelUpdateLabelDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Label', 'updateLabelDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelUpdateNotesToLabelDoBodyParam = {
      key: 'string',
      note: 'string'
    };
    describe('#updateNotesToLabelDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNotesToLabelDo(labelUpdateNotesToLabelDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Label', 'updateNotesToLabelDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loginAuthenticateDoBodyParam = {
      password: 'string',
      userId: 'string'
    };
    describe('#authenticateDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticateDo(loginAuthenticateDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.authenticationType);
                assert.equal('string', data.response.authorizationType);
                assert.equal(true, Array.isArray(data.response.permissionList));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('string', data.response.sessionId);
                assert.equal('object', typeof data.response.user);
                assert.equal('string', data.response.userName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Login', 'authenticateDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loginChangePasswordDoBodyParam = {
      contactNumber: 'string',
      currentPassword: 'string',
      email: 'string',
      firstName: 'string',
      lastName: 'string',
      oldPassword: 'string',
      userId: 'string'
    };
    describe('#changePasswordDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changePasswordDo(loginChangePasswordDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Login', 'changePasswordDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCVPUserProfileDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCVPUserProfileDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.Version);
                assert.equal('string', data.response.authenticationServerType);
                assert.equal('string', data.response.authorizationServerType);
                assert.equal(true, data.response.defaultUserFirstLogin);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Login', 'getCVPUserProfileDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissionList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Login', 'getPermissionsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#homeDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.homeDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Login', 'homeDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logoutDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logoutDo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Login', 'logoutDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rollbackAddNetworkRollbackTempActionsDoBodyParam = {
      containerId: 'string',
      endIndex: 3,
      rollbackTimestamp: 5,
      rollbackType: 'string',
      startIndex: 4,
      targetManagementIPList: [
        {}
      ]
    };
    describe('#addNetworkRollbackTempActionsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNetworkRollbackTempActionsDo(rollbackAddNetworkRollbackTempActionsDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
                assert.equal('object', typeof data.response.skippedDeviceInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rollback', 'addNetworkRollbackTempActionsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rollbackAddTempRollbackActionDoBodyParam = {
      configRollbackInput: {},
      imageRollbackInput: {},
      netElementId: 'string',
      rollbackTimestamp: 8,
      rollbackType: 'string',
      targetManagementIP: 'string'
    };
    describe('#addTempRollbackActionDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTempRollbackActionDo(rollbackAddTempRollbackActionDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rollback', 'addTempRollbackActionDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHierarchyChangeInfoDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHierarchyChangeInfoDo('fakedata', 555, null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(7, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rollback', 'getHierarchyChangeInfoDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deviceConfigs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deviceConfigs('fakedata', true, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.imageInfo);
                assert.equal('string', data.response.runningConfigInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'deviceConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotGetSnapshotChartDataDoBodyParam = {
      containerId: 'string',
      count: 4,
      interval: 'string',
      netElementId: 'string',
      templateKey: 'string',
      timeZone: 'string',
      toDate: 'string'
    };
    describe('#getSnapshotChartDataDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnapshotChartDataDo(snapshotGetSnapshotChartDataDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(3, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'getSnapshotChartDataDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotHierarchychangeinfoBodyParam = {
      deviceIDs: [
        'string'
      ]
    };
    describe('#hierarchychangeinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hierarchychangeinfo(555, snapshotHierarchychangeinfoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.hierarchyChangeInfo));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'hierarchychangeinfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSnapshotChartDataDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getgetSnapshotChartDataDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.commandList));
                assert.equal(true, Array.isArray(data.response.deviceList));
                assert.equal(6, data.response.frequency);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'getgetSnapshotChartDataDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotTemplatesBodyParam = {};
    describe('#templates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.templates(snapshotTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'templates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gettemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gettemplates(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.templateKeys));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'gettemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotTemplatesinfoBodyParam = {
      templateIDs: [
        'string'
      ]
    };
    describe('#templatesinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.templatesinfo(snapshotTemplatesinfoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.templateInfo));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'templatesinfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotSnapshotTemplatesScheduleBodyParam = {
      commands: [
        'string'
      ],
      deviceList: [
        'string'
      ],
      frequency: 3,
      name: 'string'
    };
    describe('#snapshotTemplatesSchedule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotTemplatesSchedule(snapshotSnapshotTemplatesScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.templateKey);
                assert.equal('string', data.response.templateName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'snapshotTemplatesSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotCaptureBodyParam = {
      deviceID: 'string'
    };
    describe('#capture - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.capture('fakedata', snapshotCaptureBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
                assert.equal(8, data.response.executionTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshot', 'capture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslBindCertWithCSRDoBodyParam = {
      publicCertFile: 'string',
      publicCertFileName: 'string'
    };
    describe('#bindCertWithCSRDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bindCertWithCSRDo(sslBindCertWithCSRDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.certEnabled);
                assert.equal('string', data.response.certStoreKey);
                assert.equal('string', data.response.certType);
                assert.equal('string', data.response.commonName);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.createdBy);
                assert.equal(4, data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.digestAlgorithm);
                assert.equal('string', data.response.encryptAlgorithm);
                assert.equal('string', data.response.issuedBy);
                assert.equal(5, data.response.issuedOn);
                assert.equal('string', data.response.issuedTo);
                assert.equal('string', data.response.key);
                assert.equal(6, data.response.keyLength);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.organizationUnit);
                assert.equal(true, data.response.passPhraseProtected);
                assert.equal('string', data.response.privateKeyFileName);
                assert.equal('string', data.response.publicCertFileName);
                assert.equal(false, data.response.selfSigned);
                assert.equal('string', data.response.state);
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameDNSList));
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameIPList));
                assert.equal(3, data.response.validFrom);
                assert.equal(8, data.response.validTill);
                assert.equal(9, data.response.validity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'bindCertWithCSRDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCSRDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCSRDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'deleteCSRDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProposedCertificateDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteProposedCertificateDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'deleteProposedCertificateDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportCSRDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportCSRDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.csr);
                assert.equal('string', data.response.fileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'exportCSRDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslExportCertificateDoBodyParam = {
      key: 'string',
      passPhrase: 'string'
    };
    describe('#exportCertificateDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportCertificateDo(sslExportCertificateDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cert);
                assert.equal('string', data.response.fileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'exportCertificateDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslGenerateCSRDoBodyParam = {
      commonName: 'string',
      country: 'string',
      digestAlgorithm: 'string',
      encryptAlgorithm: 'string',
      keyLength: 1,
      location: 'string',
      organization: 'string',
      organizationUnit: 'string',
      state: 'string'
    };
    describe('#generateCSRDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateCSRDo(sslGenerateCSRDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.certStoreKey);
                assert.equal('string', data.response.certType);
                assert.equal('string', data.response.commonName);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.createdBy);
                assert.equal(3, data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.digestAlgorithm);
                assert.equal('string', data.response.emailId);
                assert.equal('string', data.response.encryptAlgorithm);
                assert.equal('string', data.response.key);
                assert.equal(9, data.response.keyLength);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.organizationUnit);
                assert.equal(true, data.response.passPhraseProtected);
                assert.equal('string', data.response.state);
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameDNSList));
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameIPList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'generateCSRDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslGenerateCertificateDoBodyParam = {
      commonName: 'string',
      digestAlgorithm: 'string',
      encryptAlgorithm: 'string',
      keyLength: 8,
      validity: 2
    };
    describe('#generateCertificateDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateCertificateDo(sslGenerateCertificateDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.certEnabled);
                assert.equal('string', data.response.certStoreKey);
                assert.equal('string', data.response.certType);
                assert.equal('string', data.response.commonName);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.createdBy);
                assert.equal(3, data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.digestAlgorithm);
                assert.equal('string', data.response.encryptAlgorithm);
                assert.equal('string', data.response.issuedBy);
                assert.equal(6, data.response.issuedOn);
                assert.equal('string', data.response.issuedTo);
                assert.equal('string', data.response.key);
                assert.equal(2, data.response.keyLength);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.organizationUnit);
                assert.equal(false, data.response.passPhraseProtected);
                assert.equal('string', data.response.privateKeyFileName);
                assert.equal('string', data.response.publicCertFileName);
                assert.equal(false, data.response.selfSigned);
                assert.equal('string', data.response.state);
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameDNSList));
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameIPList));
                assert.equal(5, data.response.validFrom);
                assert.equal(9, data.response.validTill);
                assert.equal(9, data.response.validity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'generateCertificateDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCertificateDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.certEnabled);
                assert.equal('string', data.response.certStoreKey);
                assert.equal('string', data.response.certType);
                assert.equal('string', data.response.commonName);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.createdBy);
                assert.equal(1, data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.digestAlgorithm);
                assert.equal('string', data.response.encryptAlgorithm);
                assert.equal('string', data.response.issuedBy);
                assert.equal(6, data.response.issuedOn);
                assert.equal('string', data.response.issuedTo);
                assert.equal('string', data.response.key);
                assert.equal(10, data.response.keyLength);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.organizationUnit);
                assert.equal(true, data.response.passPhraseProtected);
                assert.equal('string', data.response.privateKeyFileName);
                assert.equal('string', data.response.publicCertFileName);
                assert.equal(true, data.response.selfSigned);
                assert.equal('string', data.response.state);
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameDNSList));
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameIPList));
                assert.equal(2, data.response.validFrom);
                assert.equal(10, data.response.validTill);
                assert.equal(10, data.response.validity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'getCertificateDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslImportCertAndPrivateKeyDoBodyParam = {
      certType: 'string',
      passPhrase: 'string',
      privateKey: 'string',
      privateKeyFileName: 'string',
      publicCert: 'string',
      publicCertFileName: 'string'
    };
    describe('#importCertAndPrivateKeyDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importCertAndPrivateKeyDo(sslImportCertAndPrivateKeyDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.certEnabled);
                assert.equal('string', data.response.certStoreKey);
                assert.equal('string', data.response.certType);
                assert.equal('string', data.response.commonName);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.createdBy);
                assert.equal(7, data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.digestAlgorithm);
                assert.equal('string', data.response.encryptAlgorithm);
                assert.equal('string', data.response.issuedBy);
                assert.equal(2, data.response.issuedOn);
                assert.equal('string', data.response.issuedTo);
                assert.equal('string', data.response.key);
                assert.equal(8, data.response.keyLength);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.organizationUnit);
                assert.equal(false, data.response.passPhraseProtected);
                assert.equal('string', data.response.privateKeyFileName);
                assert.equal('string', data.response.publicCertFileName);
                assert.equal(false, data.response.selfSigned);
                assert.equal('string', data.response.state);
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameDNSList));
                assert.equal(true, Array.isArray(data.response.subjectAlternateNameIPList));
                assert.equal(6, data.response.validFrom);
                assert.equal(6, data.response.validTill);
                assert.equal(2, data.response.validity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'importCertAndPrivateKeyDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installCertificateDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.installCertificateDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
                assert.equal('string', data.response.eventId);
                assert.equal('string', data.response.eventType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'installCertificateDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskExecuteTaskDoBodyParam = {
      note: 'string',
      workOrderId: 'string'
    };
    describe('#executeTaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeTaskDo(taskExecuteTaskDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'executeTaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskCancelTaskDoBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#cancelTaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelTaskDo(taskCancelTaskDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'cancelTaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskPostexecuteTaskDoBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#postexecuteTaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postexecuteTaskDo(taskPostexecuteTaskDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'postexecuteTaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ccId);
                assert.equal(10, data.response.completedOnInLongFormat);
                assert.equal('string', data.response.createdBy);
                assert.equal(10, data.response.createdOnInLongFormat);
                assert.equal('string', data.response.currentTaskName);
                assert.equal('string', data.response.currentTaskType);
                assert.equal('object', typeof data.response.data);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.executedBy);
                assert.equal(6, data.response.executedOnInLongFormat);
                assert.equal(10, data.response.factoryId);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.newParentContainerId);
                assert.equal('string', data.response.newParentContainerName);
                assert.equal('string', data.response.note);
                assert.equal('string', data.response.taskStatus);
                assert.equal('string', data.response.taskStatusBeforeCancel);
                assert.equal('string', data.response.templateId);
                assert.equal('string', data.response.workFlowDetailsId);
                assert.equal('object', typeof data.response.workOrderDetails);
                assert.equal('object', typeof data.response.workOrderEscalation);
                assert.equal('string', data.response.workOrderId);
                assert.equal('string', data.response.workOrderState);
                assert.equal('string', data.response.workOrderUserDefinedStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'getTaskByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskStatusByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskStatusByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.taskStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'getTaskStatusByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasksDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasksDo(null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(2, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'getTasksDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskAddWorkOrderLogDoBodyParam = {
      message: 'string',
      source: 'string',
      taskId: 'string'
    };
    describe('#addWorkOrderLogDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addWorkOrderLogDo(taskAddWorkOrderLogDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'addWorkOrderLogDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedCertificatesTrustedCertificatesDeleteDoBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#trustedCertificatesDeleteDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trustedCertificatesDeleteDo(trustedCertificatesTrustedCertificatesDeleteDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedCertificates', 'trustedCertificatesDeleteDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedCertificatesTrustedCertificatesExportDoBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#trustedCertificatesExportDo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.trustedCertificatesExportDo(trustedCertificatesTrustedCertificatesExportDoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cvp-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedCertificates', 'trustedCertificatesExportDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trustedCertificatesGetCertInfoDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trustedCertificatesGetCertInfoDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.certName);
                assert.equal(6, data.response.classId);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.digestAlgorithm);
                assert.equal('string', data.response.emailId);
                assert.equal('string', data.response.encryptAlgorithm);
                assert.equal('string', data.response.fingerPrint);
                assert.equal('string', data.response.issuedTo);
                assert.equal(4, data.response.keyLength);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.organizationUnit);
                assert.equal('string', data.response.signedBy);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subjectAltName);
                assert.equal('string', data.response.uploadedBy);
                assert.equal(2, data.response.validFrom);
                assert.equal(1, data.response.validTill);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedCertificates', 'trustedCertificatesGetCertInfoDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trustedCertificatesGetCertsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trustedCertificatesGetCertsDo(null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal(true, Array.isArray(data.response.trustedServers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedCertificates', 'trustedCertificatesGetCertsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedCertificatesTrustedCertificatesUploadDoBodyParam = {
      certificate: 'string'
    };
    describe('#trustedCertificatesUploadDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trustedCertificatesUploadDo(trustedCertificatesTrustedCertificatesUploadDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedCertificates', 'trustedCertificatesUploadDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAddUserDoBodyParam = {
      roles: [
        'string'
      ],
      user: {}
    };
    describe('#addUserDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addUserDo(userAddUserDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'addUserDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userDeleteUsersDoBodyParam = {};
    describe('#deleteUsersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteUsersDo(userDeleteUsersDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deleteUsersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userEnableDisableUsersDoBodyParam = {};
    describe('#enableDisableUsersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableDisableUsersDo('fakedata', userEnableDisableUsersDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'enableDisableUsersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnlineUserCountDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOnlineUserCountDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getOnlineUserCountDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUserDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersDo(null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.roles);
                assert.equal(5, data.response.total);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUsersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userUpdateUserDoBodyParam = {
      roles: [
        'string'
      ],
      user: {}
    };
    describe('#updateUserDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUserDo('fakedata', userUpdateUserDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'updateUserDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleCreateRoleDoBodyParam = {
      description: 'string',
      moduleList: [
        {}
      ],
      name: 'string'
    };
    describe('#createRoleDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRoleDo(roleCreateRoleDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.classId);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal(7, data.response.factoryId);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.key);
                assert.equal(true, Array.isArray(data.response.moduleList));
                assert.equal(7, data.response.moduleListSize);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'createRoleDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliedUserCountDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliedUserCountDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'getAppliedUserCountDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliedUsersDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliedUsersDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(10, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'getAppliedUsersDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoleDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.classId);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.createdOn);
                assert.equal('string', data.response.description);
                assert.equal(2, data.response.factoryId);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.key);
                assert.equal(true, Array.isArray(data.response.moduleList));
                assert.equal(8, data.response.moduleListSize);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'getRoleDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolesDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRolesDo(null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(5, data.response.total);
                assert.equal('object', typeof data.response.users);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'getRolesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleUpdateRoleDoBodyParam = {
      description: 'string',
      key: 'string',
      moduleList: [
        {}
      ],
      name: 'string'
    };
    describe('#updateRoleDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRoleDo(roleUpdateRoleDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'updateRoleDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleDeleteRolesDoBodyParam = {};
    describe('#deleteRolesDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRolesDo(roleDeleteRolesDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'deleteRolesDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningAddTempActionDoBodyParam = {
      data: [
        {}
      ]
    };
    describe('#addTempActionDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTempActionDo('fakedata', null, 'fakedata', provisioningAddTempActionDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.topology);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'addTempActionDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelReconcileDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelReconcileDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'cancelReconcileDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningProvisionStreamingDevicesBodyParam = {
      data: [
        {}
      ]
    };
    describe('#provisionStreamingDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.provisionStreamingDevices(provisioningProvisionStreamingDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'provisionStreamingDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningCheckComplianceDoBodyParam = {
      nodeId: 'string',
      nodeType: 'string'
    };
    describe('#checkComplianceDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.checkComplianceDo(provisioningCheckComplianceDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.architecture);
                assert.equal(2, data.response.bootupTimeStamp);
                assert.equal('string', data.response.complianceCode);
                assert.equal('string', data.response.complianceIndication);
                assert.equal('string', data.response.deviceInfo);
                assert.equal('string', data.response.deviceStatus);
                assert.equal('string', data.response.fqdn);
                assert.equal('string', data.response.hardwareRevision);
                assert.equal('string', data.response.internalBuildId);
                assert.equal('string', data.response.internalVersion);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.isDANZEnabled);
                assert.equal('string', data.response.isMLAGEnabled);
                assert.equal('string', data.response.key);
                assert.equal(4, data.response.lastSyncUp);
                assert.equal(9, data.response.memFree);
                assert.equal(4, data.response.memTotal);
                assert.equal('string', data.response.modelName);
                assert.equal('string', data.response.parentContainerId);
                assert.equal('string', data.response.serialNumber);
                assert.equal(true, data.response.sslConfigAvailable);
                assert.equal(false, data.response.sslEnabledByCVP);
                assert.equal('string', data.response.systemMacAddress);
                assert.equal(true, Array.isArray(data.response.taskIdList));
                assert.equal(true, Array.isArray(data.response.tempAction));
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.unAuthorized);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.ztpMode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'checkComplianceDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#containerLevelReconcileDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.containerLevelReconcileDo('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'containerLevelReconcileDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllTempActionDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAllTempActionDo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'deleteAllTempActionDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningPostdeleteAllTempActionDoBodyParam = {};
    describe('#postdeleteAllTempActionDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postdeleteAllTempActionDo(provisioningPostdeleteAllTempActionDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'postdeleteAllTempActionDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningDeleteTempActionDoBodyParam = {
      info: 'string',
      action: 'string',
      bestImageContainerId: 'string',
      ccId: 'string',
      childTasks: [
        10
      ],
      configCompareCount: {},
      configletBuilderList: [
        'string'
      ],
      configletBuilderNamesList: [
        'string'
      ],
      configletList: [
        'string'
      ],
      configletNamesList: [
        'string'
      ],
      containerKey: 'string',
      fromId: 'string',
      fromName: 'string',
      ignoreConfigletBuilderList: [
        'string'
      ],
      ignoreConfigletBuilderNamesList: [
        'string'
      ],
      ignoreConfigletList: [
        'string'
      ],
      ignoreConfigletNamesList: [
        'string'
      ],
      ignoreNodeId: 'string',
      ignoreNodeList: [
        'string'
      ],
      ignoreNodeName: 'string',
      ignoreNodeNamesList: [
        'string'
      ],
      imageBundleId: 'string',
      infoPreview: 'string',
      key: 'string',
      mode: 'string',
      nodeId: 'string',
      nodeIpAddress: 'string',
      nodeList: [
        'string'
      ],
      nodeName: 'string',
      nodeNamesList: [
        'string'
      ],
      nodeTargetIpAddress: 'string',
      nodeType: 'string',
      note: 'string',
      oldNodeName: 'string',
      pageType: 'string',
      parentTask: 'string',
      sessionId: 'string',
      taskId: 7,
      timestamp: 5,
      toId: 'string',
      toIdType: 'string',
      toName: 'string',
      userId: 'string',
      viaContainer: true
    };
    describe('#deleteTempActionDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTempActionDo(provisioningDeleteTempActionDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.Result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'deleteTempActionDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterTopologyDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterTopologyDo(null, null, 'fakedata', 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.topology);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'filterTopologyDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTempActionsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTempActionsDo(555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(8, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getAllTempActionsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletsByContainerIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletsByContainerIdDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.configletList));
                assert.equal('object', typeof data.response.configletMapper);
                assert.equal(10, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getConfigletsByContainerIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigletsByNetElementIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigletsByNetElementIdDo('fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.configletList));
                assert.equal('object', typeof data.response.configletMapper);
                assert.equal(10, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getConfigletsByNetElementIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#topologyDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.topologyDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.associatedConfiglets);
                assert.equal(3, data.response.associatedSwitches);
                assert.equal('string', data.response.bundleName);
                assert.equal(10, data.response.danzEnabledSwitches);
                assert.equal(1, data.response.date);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.parentName);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'topologyDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundleByContainerIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageBundleByContainerIdDo('fakedata', 'fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.imageBundleList));
                assert.equal('object', typeof data.response.imageBundleMapper);
                assert.equal(1, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getImageBundleByContainerIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageBundleByNetElementIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageBundleByNetElementIdDo('fakedata', 'fakedata', null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.imageBundleList));
                assert.equal('object', typeof data.response.imageBundleMapper);
                assert.equal(10, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getImageBundleByNetElementIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageForTaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageForTaskDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.appliedContainersCount);
                assert.equal(7, data.response.appliedDevicesCount);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.images));
                assert.equal('string', data.response.isCertifiedImage);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.updatedTimeInLongFormat);
                assert.equal('string', data.response.uploadedBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getImageForTaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabelsByNetElementIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLabelsByNetElementIdDo(null, 555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.labelList));
                assert.equal(4, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getLabelsByNetElementIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetElementByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetElementByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.architecture);
                assert.equal(10, data.response.bootupTimeStamp);
                assert.equal('string', data.response.complianceCode);
                assert.equal('string', data.response.complianceIndication);
                assert.equal('string', data.response.deviceInfo);
                assert.equal('string', data.response.deviceStatus);
                assert.equal('string', data.response.fqdn);
                assert.equal('string', data.response.hardwareRevision);
                assert.equal('string', data.response.internalBuildId);
                assert.equal('string', data.response.internalVersion);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.isDANZEnabled);
                assert.equal('string', data.response.isMLAGEnabled);
                assert.equal('string', data.response.key);
                assert.equal(2, data.response.lastSyncUp);
                assert.equal(10, data.response.memFree);
                assert.equal(4, data.response.memTotal);
                assert.equal('string', data.response.modelName);
                assert.equal('string', data.response.parentContainerId);
                assert.equal('string', data.response.serialNumber);
                assert.equal(true, data.response.sslConfigAvailable);
                assert.equal(false, data.response.sslEnabledByCVP);
                assert.equal('string', data.response.systemMacAddress);
                assert.equal(true, Array.isArray(data.response.taskIdList));
                assert.equal(true, Array.isArray(data.response.tempAction));
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.unAuthorized);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.ztpMode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getNetElementByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetElementImageByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetElementImageByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getNetElementImageByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetElementInfoByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetElementInfoByIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.bundleName);
                assert.equal('string', data.response.eosVersion);
                assert.equal('string', data.response.fqdn);
                assert.equal('string', data.response.imageBundleId);
                assert.equal('object', typeof data.response.imageBundleMapper);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.serialNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getNetElementInfoByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetElementListDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetElementListDo('fakedata', null, 555, 555, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.containerList));
                assert.equal(true, Array.isArray(data.response.netElementList));
                assert.equal(9, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getNetElementListDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProposedContainerDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProposedContainerDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getProposedContainerDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTempConfigsByContainerIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTempConfigsByContainerIdDo('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assignedConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.assignedConfiglets));
                assert.equal(true, Array.isArray(data.response.existingConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.existingConfiglets));
                assert.equal(true, Array.isArray(data.response.ignoredConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.ignoredConfiglets));
                assert.equal(true, Array.isArray(data.response.proposedConfiglets));
                assert.equal('string', data.response.tempConfigletObjectIdMapper);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getTempConfigsByContainerIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTempConfigsByNetElementIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTempConfigsByNetElementIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assignedConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.assignedConfiglets));
                assert.equal(true, Array.isArray(data.response.deviceConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.deviceConfiglets));
                assert.equal(true, Array.isArray(data.response.existingConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.existingConfiglets));
                assert.equal(true, Array.isArray(data.response.ignoredConfigletBuilders));
                assert.equal(true, Array.isArray(data.response.ignoredConfiglets));
                assert.equal(true, Array.isArray(data.response.proposedConfiglets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getTempConfigsByNetElementIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTempLabelsByNetElementIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTempLabelsByNetElementIdDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assignedLabels));
                assert.equal(true, Array.isArray(data.response.existingLabels));
                assert.equal(true, Array.isArray(data.response.ignoredLabels));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getTempLabelsByNetElementIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTempSessionReconciledConfigletsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTempSessionReconciledConfigletsDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getTempSessionReconciledConfigletsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getconfigfortaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getconfigfortaskDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.configs));
                assert.equal(true, Array.isArray(data.response.designedConfig));
                assert.equal('string', data.response.errors);
                assert.equal(true, data.response.isReconcileInvoked);
                assert.equal(4, data.response.mismatch);
                assert.equal(8, data.response.new);
                assert.equal(8, data.response.reconcile);
                assert.equal('string', data.response.reconciledConfig);
                assert.equal(true, Array.isArray(data.response.runningConfig));
                assert.equal('string', data.response.targetIpAddress);
                assert.equal(8, data.response.total);
                assert.equal(true, Array.isArray(data.response.warning));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'getconfigfortaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningIpConnectivityTestDoBodyParam = {
      ipAddress: 'string'
    };
    describe('#ipConnectivityTestDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipConnectivityTestDo(provisioningIpConnectivityTestDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'ipConnectivityTestDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningSaveTopologyDoBodyParam = {};
    describe('#saveTopologyDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saveTopologyDo(provisioningSaveTopologyDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'saveTopologyDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchTopologyDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchTopologyDo(null, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.containerList));
                assert.equal(true, Array.isArray(data.response.keywordList));
                assert.equal(true, Array.isArray(data.response.netElementContainerList));
                assert.equal(true, Array.isArray(data.response.netElementList));
                assert.equal(8, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'searchTopologyDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetElementImageByIdDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetElementImageByIdDo('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'updateNetElementImageByIdDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningUpdateReconcileConfigletDoBodyParam = {
      config: 'string',
      name: 'string',
      reconciled: true
    };
    describe('#updateReconcileConfigletDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReconcileConfigletDo('fakedata', provisioningUpdateReconcileConfigletDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'updateReconcileConfigletDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#v2GetconfigfortaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.v2GetconfigfortaskDo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.Total);
                assert.equal(true, Array.isArray(data.response.configs));
                assert.equal(true, Array.isArray(data.response.designedConfig));
                assert.equal(true, Array.isArray(data.response.errors));
                assert.equal(4, data.response.mismatch);
                assert.equal(9, data.response.new);
                assert.equal(4, data.response.reconcile);
                assert.equal('string', data.response.reconciledConfig);
                assert.equal(true, Array.isArray(data.response.runningConfig));
                assert.equal('string', data.response.targetIpAddress);
                assert.equal(true, Array.isArray(data.response.warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'v2GetconfigfortaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningSaveTopologyDo1BodyParam = {};
    describe('#saveTopologyDo1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saveTopologyDo1(provisioningSaveTopologyDo1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'saveTopologyDo1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const provisioningValidateAndCompareConfigletsDoBodyParam = {
      configIdList: [
        'string'
      ],
      netElementId: 'string'
    };
    describe('#validateAndCompareConfigletsDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateAndCompareConfigletsDo(provisioningValidateAndCompareConfigletsDoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.designedConfig));
                assert.equal(true, Array.isArray(data.response.errors));
                assert.equal(true, data.response.isReconcileInvoked);
                assert.equal(5, data.response.mismatch);
                assert.equal(2, data.response.new);
                assert.equal(4, data.response.reconcile);
                assert.equal('object', typeof data.response.reconciledConfig);
                assert.equal(true, Array.isArray(data.response.runningConfig));
                assert.equal(4, data.response.total);
                assert.equal(true, Array.isArray(data.response.warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'validateAndCompareConfigletsDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#v3GetconfigfortaskDo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.v3GetconfigfortaskDo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.configCapturedAt);
                assert.equal(true, Array.isArray(data.response.configs));
                assert.equal(true, Array.isArray(data.response.designedConfig));
                assert.equal(true, Array.isArray(data.response.errors));
                assert.equal(false, data.response.isReconcileInvoked);
                assert.equal(3, data.response.mismatch);
                assert.equal(3, data.response.new);
                assert.equal(9, data.response.reconcile);
                assert.equal(true, Array.isArray(data.response.runningConfig));
                assert.equal(10, data.response.runningConfigcount);
                assert.equal('string', data.response.targetIpAddress);
                assert.equal(6, data.response.total);
                assert.equal(true, Array.isArray(data.response.warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Provisioning', 'v3GetconfigfortaskDo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
