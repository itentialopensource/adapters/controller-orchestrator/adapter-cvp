
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!2

---

## 0.1.2 [07-13-2021]

- Need to remove the update files since they have an additional pronghorn.json in them

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!1

---

## 0.1.1 [03-24-2021]

- Initial Commit

See commit 392c20d

---
