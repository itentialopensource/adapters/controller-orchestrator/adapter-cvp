# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Cvp System. The API that was used to build the adapter for Cvp is usually available in the report directory of this adapter. The adapter utilizes the Cvp API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The CloudVision Portal adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Arista CloudVision Portal. 

With this adapter you have the ability to perform operations with CloudVision Portal such as:

- Get Inventory
- Execute Confliglet
- Get Event
- Provision

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
