
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:35PM

See merge request itentialopensource/adapters/adapter-cvp!13

---

## 0.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cvp!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:50PM

See merge request itentialopensource/adapters/adapter-cvp!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:03PM

See merge request itentialopensource/adapters/adapter-cvp!9

---

## 0.4.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!8

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:41PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!7

---

## 0.3.3 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:58PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:27AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!4

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!3

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!2

---

## 0.1.2 [07-13-2021]

- Need to remove the update files since they have an additional pronghorn.json in them

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cvp!1

---

## 0.1.1 [03-24-2021]

- Initial Commit

See commit 392c20d

---
